Router.configure({
	layoutTemplate: 'layout',
	loadingTemplate: 'loading',
	notFoundTemplate: 'notFound'
});

Router.route('/', {
	name: 'home', 
	template: 'home'
});

Router.route('/upload', {
	name: 'upload',
	template: 'upload'
});

Router.onBeforeAction('loading');