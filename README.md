To run the project:

- Checkout the project
- Go to the root folder and create a file `settings.json`
- Go to the root folder and run `meteor --settings settings.json`
    - This file should contain the amazon AWS S3 keys to store the images
- The project should run!

How it works:

- Visit `/upload` on a mobile device (or not) and upload a picture
- Return to the homepage (root index) to view your image and some interesting 
metadata which we extracted on the server.

Enjoy!
