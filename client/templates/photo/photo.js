Template.photo.events({
    'change #file': function (event) {
        FS.Utility.eachFile(event, function (file) {
            var newFile = new FS.File(file);

            Images.insert(newFile, function (error, fileObj) {
                if (error) {
                    toastr.error("Upload failed... please try again.");
                } else {
                    toastr.success('Upload succeeded!');
                }
            });
        });
    }
});